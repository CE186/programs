class Vehicle:
    def __init__(self, name, color):
        self.name = name
        self.color = color
    def show(self):
        print(f"This vehicle  is {self.color} color {self.name}")

#super is used to call the parent class
class Bike(Vehicle):
    def __init__(self, name, color, age):
        super().__init__(name, color)
        self.age = age
        
    def show(self):
        print(f"This vehicle  is {self.color} color {self.name} and I am {self.age}")
        
    
    
    def sound(self):
        print("Zoooommm")
        

        
class Car(Vehicle):
    def sound(self):
        print("Vrrrooom")
    
    
c1 = Car("Bugatti", "Red")
c1.show()
c1.sound()
c2 = Car
b1 = Bike("Pulsar", "Blue", 34)
b1.show()
