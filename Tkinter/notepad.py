from tkinter import *
from tkinter.messagebox import showinfo
from tkinter.filedialog import askopenfilename, asksaveasfilename
import os

def newFile():
    global file
    root.title('Untitled - Notepad')
    file = None
    TextArea.delete(1.0,END)

def openFile():
    global file
    file = askopenfilename(defaultextension='.txt',
                           filetypes=[('All Files', '*.*'),
                                      ('Text Documents','*.txt')])
    if file == "":
        file = None 
    else:
        root.title(os.path.basename(file) + '- Notepad')
        TextArea.delete(1.0,END)
        
        f = open(file, 'r')
        TextArea.insert(1.0,f.read())
        f.close()

def saveFile():
    global file 
    if file == None:
        file = asksaveasfilename(initialfile = 'Untitled.txt', defaultextension='.txt',
                                 filetypes=[('All Files', '*.*'),
                                      ('Text Documents','*.txt')])
        
        
        if file == "":
            file = None 
            
        else:
            f = open(file , 'w')
            f.write(TextArea.get(1.0,END))
            f.close()
            root.title(os.path,basename(file) + ' - Notepad' )
        
    

def exitFile():
    root.destroy()

def cut():
    TextArea.event_generate(("<<Cut>>"))
def copy():
    TextArea.event_generate(("<<Copy>>"))
def paste():
    TextArea.event_generate(("<<Paste>>"))


def about():
    showinfo("Notepad", "Jay's Notepad")

if __name__ == "__main__":
    #tkinter setup
    
    root = Tk()
    root.title('Untitled- Notepad')
    root.geometry('444x588')
    
    #adding a text area
    
    TextArea = Text(root, font='Lucidia 15')
    TextArea.pack(fill=BOTH, expand=True)
    file = None 
    
    #creating a menubar
    MenuBar = Menu(root)
    
    #creating file menu
    FileMenu = Menu(MenuBar, tearoff=0)
    
    #TO open a new file
    FileMenu.add_command(label = 'New', command=newFile)
    #TO oprn a file
    FileMenu.add_command(label='Open',command=openFile)
    # Saving the file
    FileMenu.add_command(label='Save', command=saveFile)
    FileMenu.add_separator()
    FileMenu.add_command(label='Exit', command=exitFile)
    MenuBar.add_cascade(label='File', menu=FileMenu)
    
    
    
    #edit menu starts 
    EditMenu = Menu(MenuBar,tearoff=0)
    #TO give cut
    EditMenu.add_command(label='Cut', command=cut)
    #TO give copy
    EditMenu.add_command(label='Copy', command=copy)
    #TO give paste
    EditMenu.add_command(label='Paste', command=paste)
    MenuBar.add_cascade(label='Edit', menu=EditMenu)
    #Edit menu ends
    
    #Start Help MEnu
    HelpMenu = Menu(MenuBar, tearoff=0)
    HelpMenu.add_command(label='About', command=about)
    MenuBar.add_cascade(label='Help', menu=HelpMenu)
    
    root.config(menu = MenuBar)
    
    
    #adding a scrollbar
    Scroll = Scrollbar(TextArea)
    Scroll.pack(side=RIGHT, fill=Y)
    Scroll.config(command=TextArea.yview)
    TextArea.config(yscrollcommand=Scroll.set)
    
    root.mainloop()