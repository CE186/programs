from flask import *
app = Flask(__name__)

@app.route("/<string:name>")
def hello(name):
    return "Hello" + name

if __name__ == "__main__":
    app.run()