class Car:
    pass
ford = Car()
audi = Car()
ford.speed = 250
audi.speed = 300
ford.color = 'red'
audi.color = 'black'

print(ford.speed,ford.color,audi.speed,audi.color)

class Bike:
    def __init__(self,color,speed):
    
         print(color)
         print(speed)
         #Self provides the attribute
         self.__speed = speed;
         self.color = color;
    def set_speed(self,value):
        self.__speed = value
    def get_speed(self):
        return self.__speed

bmw = Bike('red',250)
bmw.set_speed(300)
bmw.speed = 500
print(bmw.get_speed())


class Animal:
    def __init__(self,weight,__type):
        print(weight)
        print(__type)
        self.weight = weight;
        self.__type = __type;
cat = Animal(100,'cat')
cat.weight = 150
cat.__type = 'bat'
print(cat.weight)
print(cat.__type)

