class Coffee:
    def __init__(self,temperature):
        self.temperature = temperature
    def drinkCoffee(self):
        if self.temperature > 80:
            raise Exception("Too Hot")
            # print("Coffee is to hot to drink")
        elif self.temperature < 60:
            raise Exception("Too Cold")
            # print("Coffee is too cold to drink ")
        else:
            print("Thanks for the coffee")

coffee = Coffee(6)
coffee.drinkCoffee()        