class Polygon:
    width = None
    height = None

    def set_values(self,width,height):
        self.width = width
        self.height = height

class Rectangle(Polygon):
    def area(self):
        return self.width * self.height

rect = Rectangle()
rect.set_values(40,20)
print(rect.area())