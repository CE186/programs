class car:
    def __init__(self,speed,color):
        self.__speed = speed;
        self.__color = color;
    def set_speed(self,value):
        self.__speed = value;
    def get_speed(self):
        return self.__speed
ford = car(250,'red')


#the value cannot be changed
ford.speed = 150

print(ford.get_speed())

