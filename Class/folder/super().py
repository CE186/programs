class Parent:
    def __init__(self):
        print("__init__ as parent ")

class Child(Parent):
    def __init__(self):
        print("__init__ as child ") 

child = Child()

class Parent:
    def __init__(self,name):
        print("__init__ as parent ", name)
    
class Child(Parent):
    def __init__(self):
        print("__init__ as child ")
        super().__init__('Maxx')

child = Child()

