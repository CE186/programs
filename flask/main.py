from flask import *
app = Flask(__name__)

@app.route("/")
def homepage():
    return render_template("index.html")

@app.route("/index")
def home():
    return render_template("index.html")

@app.route("/about")
def about():
    return render_template("about.html")

@app.route("/contact")
def contactme():
    return render_template("contact.html")

app.run(debug=True)