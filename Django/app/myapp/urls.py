from django.contrib import admin
from django.urls import path
from myapp import views

urlpatterns = [
    path('', views.index, name='homepage'),
    path('contact/', views.contact, name='contactus'),
    path('about/', views.about, name='about'),
]
