import requests
import bs4
from bs4 import *

url = 'https://codewithharry.com'

data = requests.get(url)

htmlcontent = data.content

soup = BeautifulSoup(htmlcontent, 'html.parser')
# title = soup.title
# print(title.string)


para = soup.find_all('p')
# print(para)
# print(soup.prettify())

# print(anchor)


# print(soup.find('p')['class'])


allclass = soup.find_all(('p'), class_='lead')
# print(allclass)



#getting the text
# print(soup.find('p').get_text)
# print(soup.get_text())



# anchor = soup.find_all('a')
# for link in anchor:
#     print(link.get('href'))



anchor = soup.find_all('a')
all_links = set()
for link in anchor:
    if (link.get("href") != "#" and link.get("href") != '/'):
        links = "https://codewithharry.com" + link.get('href')
        all_links.add(links)
        print(links)