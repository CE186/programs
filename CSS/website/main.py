from flask import *
app = Flask(__name__)

@app.route("/")
def hpmepage():
    return render_template("layout.html")

@app.route("/home")
def mainscreen():
    return render_template("body.html")

@app.route("/contact")
def contact():
    return render_template("contact.html")

@app.route("/connect")
def connect():
    return render_template("connect.html")




app.run(debug=True)